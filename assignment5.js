import express from 'express'
import mysql from 'mysql2'
import dotenv from 'dotenv'
dotenv.config()
const app = express()

app.use(express.json());


const port = process.env.PORT_NUMBER;

const pool = mysql.createPool({
    host: process.env.MYSQL_HOST,
    user: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    database: "backend_assignment5"
}).promise()

async function all() {
    const [row] = await pool.query("SELECT * FROM users")
    return row
}

async function byId(id) {
    const [row] = await pool.query(`
    SELECT * 
    FROM users
    where id = ? 
    `, [id])
    return row[0]
}
async function createContact(first_name, last_name, phone_number) {
    const [result] = await pool.query(`INSERT INTO
    users VALUES(? ,?,?)`, [first_name, last_name, phone_number]
    )
    const id = result.insertId
    return byId(id)
}
async function deletContact(id) {
    const [row] = await pool.query(`DELETE FROM users WHERE id = ?`, [id])
    return row
}

async function updateContact(first_name, last_name, phone_number) {
    const [result] = await pool.query(`UPDATE users SET first_name = ? ,last_name = ? ,phone_number =? `, 
    [first_name, last_name, phone_number])
}



app.post("./contact", async (req, res) => {
    const {first_name, last_name, phone_number } = req.body
    const showCreatedContact = await createContact(first_name, last_name, phone_number)
    res.status(201).json(showCreatedContact)
})

app.get("/contact/:id", async (req, res) => {
    const id = req.params.id
    const showById = await byId(id)
    res.send(showById)
})

app.put("/contact/:id", async (req, res) => {
    const {first_name, last_name, phone_number } = req.body
    const showUpdate = await updateContact(first_name, last_name, phone_number)
    res.json(showUpdate)
})
app.delete("/contact/:id", async (req, res) => {
    const id = req.params.id
    await deletContact(id)
    res.send('Contact deletec successfully.')
})

app.get("/contact", async (req, res) => {
    const showAll = await all()
    res.send(showAll)
})

app.use(async (err, req, res, next) => {
    console.error(err.stack)
    res.status(500).send('Something broke')
}
)
app.listen(3000, () => {
    console.log('server is running  ')
})          